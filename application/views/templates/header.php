<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/img/favicons/favicon.ico">

    <title>Fixed top navbar example for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbar-fixed/">

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">

    <!-- Custom styles for this template -->
  </head>
  <body>
    <nav class="navbar own fixed-top navbar-expand-sm navbar-light bg-light">
    <div class="col-0 col-md-0">
        <img src="/img/emblem.png" alt="logo">
      </div>
      <div class="col-12 col-md-12">
      <h3 class="listname">Councilors List</h3>
        <sub>Collection of all the information about the councilors</sub>
      </div>
    </nav>
    <nav class="navbar own2 navbar-expand-sm fixed-top navbar-dark bg-dark">
        <a class="nav-link" href="#">Dashboard</a>
        <a class="nav-link" href="<?php echo base_url();?>clist">List</a>
        <a class="nav-link" href="<?php echo base_url();?>add">Add</a>
        <a class="nav-link" href="#">Roles</a>
        <a class="nav-link" href="#">Permissions</a>
    </nav>
    <main role="main" class="section cmain">
    
   