<?php

    class Add extends CI_Controller{
         public function __construct(){
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->model('addModel');
         }
        
        public function index(){
            $this->load->view('templates/header');
            $this->load->view('pages/add');
            $this->load->view('templates/footer');

        }

        public function staff_add(){
                $this->form_validation->set_rules("name_en", "Name", 'required');
                $this->form_validation->set_rules("name_dh", "ނަން", 'required');
                $this->form_validation->set_rules("address_en", "Address", 'required');
                $this->form_validation->set_rules("address_dh", "Address", 'required');
                $this->form_validation->set_rules("id_card", "ID Card", 'required');
                $this->form_validation->set_rules("mb_number", "Mobile Number", 'required');
                $this->form_validation->set_rules("of_number", "Office Number", 'required');
                $this->form_validation->set_rules("gender", "Gender", 'required');
                $this->form_validation->set_rules("gender_dv", "Gender", 'required');
                $this->form_validation->set_rules("politicalparty", "Political Party", 'required');
                $this->form_validation->set_rules("design_id", "Designation ID", 'required');

            if($this->form_validation->run()==FALSE)
            {
                    $this->load->view('templates/header');
                    $this->load->view('pages/add');
                    $this->load->view('templates/footer');
            } else {
                    $name = $this->input->post('name_en');
                    $namedhi = $this->input->post('name_dh');
                    $address = $this->input->post('address_en');
                    $addressdhi = $this->input->post('address_dh');
                    $idcard = $this->input->post('id_card');
                    $mbnumber = $this->input->post('mb_number');
                    $ofnumber = $this->input->post('of_number');
                    $genders = $this->input->post('gender');
                    $genders_dv = $this->input->post('gender_dv');
                    $polparty = $this->input->post('politicalparty');
                    $designateid = $this->input->post('design_id');
        
                    $data = array(
                        'name_eng' => $name,
                        'name_dhi' => $namedhi,
                        'p_address_eng' => $address,
                        'p_address_dhi' => $addressdhi,
                        'id_card' => $idcard,
                        'mobile_number' => $mbnumber,
                        'office_number' => $ofnumber,
                        'gender' => $genders,
                        'gender_dv' => $genders_dv,
                        'political_party' => $polparty,
                        'designation_id' => $designateid
                    );
                    
                    $this->addModel->addstaff($data);
                    redirect('clist');
                }
            }
        }