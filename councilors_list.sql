-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 28, 2019 at 03:55 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `councilors_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `council`
--

CREATE TABLE `council` (
  `id` int(11) NOT NULL,
  `atoll_eng` varchar(255) NOT NULL,
  `atoll_dhi` varchar(255) NOT NULL,
  `island_eng` varchar(255) NOT NULL,
  `c_name_eng` varchar(255) NOT NULL,
  `c_name_dhi` varchar(255) NOT NULL,
  `business_area` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `designation_eng` varchar(255) NOT NULL,
  `designation_dhi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `designation_eng`, `designation_dhi`) VALUES
(1, 'President', 'ރައީސް'),
(3, 'vice president', 'ވައިސް ޕްރެޒިޑެންޓް'),
(4, 'Member', 'މެމްބަރ'),
(5, 'Director', 'ޑިރެކްޓަރ'),
(6, 'Secretary General', 'ސެކްރޭޓްރީ ޖެނެރަލް');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `designation_id` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `term_end_date` varchar(255) NOT NULL,
  `current_position` tinyint(1) NOT NULL,
  `reason_leaving` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL DEFAULT '4',
  `name_eng` varchar(255) NOT NULL,
  `name_dhi` varchar(255) NOT NULL,
  `p_address_eng` varchar(255) NOT NULL,
  `p_address_dhi` varchar(255) NOT NULL,
  `id_card` varchar(20) NOT NULL,
  `mobile_number` int(10) NOT NULL,
  `office_number` int(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `political_party` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `designation_id`, `name_eng`, `name_dhi`, `p_address_eng`, `p_address_dhi`, `id_card`, `mobile_number`, `office_number`, `gender`, `political_party`) VALUES
(1, 3, 'Ahmed Sunil', 'އަހުމަދު ސުނިލް', 'Marine Villa', 'މެރިންވިލާ', 'A316319', 7626626, 7626626, 'Male', 'NOPT'),
(2, 1, 'Ali Ahmed', 'އަލީ އަހުމަދު', 'Jamaluge', 'ޖަމަލުގެ', 'A341123', 76123089, 2893408, 'Male', 'MDP'),
(3, 5, 'Adam Suhail', 'އާދަމް ސުހައިލް', 'Gray Cloud', 'އަލވިލާ', 'A123123', 7626623, 1234123, 'Male', 'NOPT'),
(4, 6, 'Ahmed Jamalu', 'އަހުމަދު ޖަމަލު', 'Marineahmed', 'މެރިންއަހުމަދު', 'A123141', 8912803, 1231234, 'Female', 'NODP'),
(5, 1, 'Mohamed Ibrahim', 'މުހައްމަދު އިބްރާހިމް', 'Villaage', 'ވިލާގެ', 'A779112', 812783, 12324234, 'Male', 'MDP');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `council`
--
ALTER TABLE `council`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `council`
--
ALTER TABLE `council`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
