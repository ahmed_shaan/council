<?php
    class CLists extends CI_Controller{
       public function  __construct(){
         parent::__construct();
         $this->load->model('ClistModel');
       }

       public function clists(){
            $data['staffs'] = $this->ClistModel->get_list();
            // print_r($this->db->last_query());    
            // exit;
            $this->load->view('templates/header');
            $this->load->view('pages/clist', $data);
            $this->load->view('templates/footer');
       }
    }