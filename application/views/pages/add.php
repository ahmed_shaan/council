<div class="section">
<div class="col-md-10 mx-auto">
        <div class="card own5">
        <div class="card-body">
        <?php echo form_open('add/staff_add'); ?>
            <h5>Personal</h5>
            <hr>
                    <div class="form-group row">
                        <div class="col-sm-3">
                        <label for="name">Name</label>
                        <input type="text" name="name_en" class="form-control" id="name_en">
                        <?php echo form_error('name_en', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="name_dh" class="float-right">ނަން</label>
                        <input type="text" name="name_dh" class="form-control" id="name_dv">
                        <?php echo form_error('name_dh', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="address">Address</label>
                        <input type="text" name="address_en" class="form-control" id="address_en">
                        <?php echo form_error('address_en', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="address_dh" class="float-right">އެޑްރެސް</label>
                        <input type="text" name="address_dh"class="form-control" id="address_dv">
                        <?php echo form_error('address_dh', '<p style="color:red">', '</p>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                        <label for="id_card">ID Card</label>
                        <input type="text" name="id_card" class="form-control" id="id_cardx">
                        <?php echo form_error('id_card', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="mb_number">Mobile Number</label>
                        <input type="text" name="mb_number" class="form-control" id="mbNum">
                        <?php echo form_error('mb_number', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="gender">Gender</label>
                            <!-- <input type="text" name="gender" class="form-control" id=""> -->
                            <?php echo form_error('gender', '<p style="color:red">', '</p>'); ?>
                            <select class="custom-select" name="gender">
                                <option selected>Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select>
                            <?php echo form_error('gender', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="gender" class="float-right">ޖިންސް</label>
                            <!-- <input type="text" name="gender" class="form-control" id=""> -->
                            <?php echo form_error('gender', '<p style="color:red">', '</p>'); ?>
                            <select class="custom-select text-right" name="gender_dv">
                                <option selected>ޖިންސް</option>
                                <option value="Male">ފިރިހެން</option>
                                <option value="Female">އަންހެން</option>
                                <option value="Other">އެހެނިހެން</option>
                            </select>
                            <?php echo form_error('gender', '<p style="color:red">', '</p>'); ?>
                        </div>
                    </div>
                </div>
                </div>
                    <br>
                    <div class="card own5">
                    <div class="card-body">
                    <h4>Council and Designation</h4>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="of_number">Office Number</label>
                            <input type="text" name="of_number" class="form-control" id="ofNum">
                            <?php echo form_error('of_number', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                            <label for="politicalparty" class="float-right">މަޤާމް</label>
                            <select class="custom-select" name="designation_dv">
                            <option selected>މަޤާމް</option>
                            <option value="1">ރައީސް</option>
                            <option value="5">ޑިރެކްޓަރ</option>
                            <option value="6">ސެކްރެޓެރީ ޖެނެރަލް</option>
                            <option value="3">ނައިބު ރައީސް</option>
                            <option value="4">މެމްބަރު</option>
                        </select>
                        <?php echo form_error('designation_dv', '<p style="color:red">', '</p>'); ?>
                        </div>
                        <div class="col-sm-3">
                        <label for="design_id">Designation</label>
        <!-- <input type="text" name="design_id" class="form-control" id=""> -->
                        <select class="custom-select" name="design_id">
                            <option selected>Select Designation</option>
                            <option value="1">President</option>
                            <option value="5">Director</option>
                            <option value="6">Secretary General</option>
                            <option value="3">Vice President</option>
                            <option value="4">Member</option>
                        </select>
                        <?php echo form_error('design_id', '<p style="color:red">', '</p>'); ?>
                        </div>
                    <div class="col-sm-3">
                        <label for="design_id">Atoll</label>
        <!-- <input type="text" name="design_id" class="form-control" id=""> -->
                        <select class="custom-select" name="design_id">
                            <option selected>Select Atoll</option>
                            <option value="1">President</option>
                            <option value="5">Director</option>
                            <option value="6">Secretary General</option>
                            <option value="3">Vice President</option>
                            <option value="4">Member</option>
                        </select>
                        <?php echo form_error('atoll', '<p style="color:red">', '</p>'); ?>
                        </div>
                    </div>
                 </div>
                </div>
                <button type="submit" class="btn btn-primary mt-2 mb-2 px-4 float-right">Save</button>
                </form>
            </div>
        </div>
