<?php
    class ClistModel extends CI_Model{
        public function get_list(){
            $this->db->select('designation.id,designation_eng,designation_dhi, name_eng,name_dhi,p_address_eng,p_address_dhi,id_card,mobile_number,office_number,gender,political_party');
            $this->db->join('designation', 'designation.id = staff.designation_id');
            $query = $this->db->get('staff');
            $row = $query->result_array();
            return $row;
        }
    }